import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArenaRoutingModule } from './arena-routing.module';
import { HomeComponent } from './home/home.component';
import { ArenaComponent } from './arena.component';


@NgModule({
  declarations: [
    HomeComponent,
    ArenaComponent
  ],
  imports: [
    CommonModule,
    ArenaRoutingModule
  ]
})
export class ArenaModule { }
